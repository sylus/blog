<?php
session_start();
require 'config.php';
require '../functions.php';
comprobarSesion();
$conexion = conexion($bd_config);
if (!$conexion) {
    header('Location: '.RUTA.'error.php');
}
$id = limpiarDatos($_GET['id']);
if (!$id) {
    header('Location: '.RUTA.'admin');
}
$st = $conexion->prepare('DELETE FROM t_articulos WHERE id_articulo = :id');
$st->execute(array(':id' => $id));
header('Location: '.RUTA.'admin');
?>