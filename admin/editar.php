<?php
session_start();
require 'config.php';
require '../functions.php';
comprobarSesion();
$conexion = conexion($bd_config);
if (!$conexion) {
    header('Location: '.RUTA.'error.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $titulo = limpiarDatos($_POST['titulo']);
    $extracto = limpiarDatos($_POST['extracto']);
    $texto = $_POST['texto'];
    $id = $_POST['id_articulo'];
    $thumb_guardada = $_POST['thumb-guardada'];
    $thumb = $_FILES['thumb'];
    if (empty($thumb['name'])) {
        $thumb = $thumb_guardada;
    } else {
        $archivo_subido = '../'.$blog_config['carpetaImg'].$_FILES['thumb']['name'];
        move_uploaded_file($_FILES['thumb']['tmp_name'], $archivo_subido);
        $thumb = $_FILES['thumb']['name'];
    }
    $st = $conexion->prepare('UPDATE t_articulos SET titulo = :titulo, extracto = :extracto, texto = :texto, thumb = :thumb WHERE id_articulo = :id');
    $st->execute(array(':titulo' => $titulo, ':extracto' => $extracto, ':texto' => $texto, ':thumb' => $thumb, ':id' => $id));
    header('Location: '.RUTA.'admin');
} else {
    $id_articulo = id_articulo($_GET['id']);
    if (empty($id_articulo)) {
        header('Location: '.RUTA.'admin');
    }
    $post = obtenerPostPorId($conexion, $id_articulo);
    if (!$post) {
        header('Location: '.RUTA.'admin');
    }
    $post = $post[0];
}
require '../views/editar.view.php';