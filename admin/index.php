<?php
session_start();
//Archivo index del admin

require 'config.php';
require '../functions.php';
$conexion = conexion($bd_config);
comprobarSesion();
if (!$conexion) {
     header('Location: ../error.php');
}
$posts = obtenerPost($blog_config['postPorPagina'], $conexion);

require '../views/admin_index.view.php';