<?php
session_start();
require 'config.php';
require '../functions.php';
comprobarSesion();
$conexion = conexion($bd_config);
if (!$conexion) {
    header('Location: '.RUTA.'error.php');
}
if ($_SERVER['REQUEST_METHOD'] =='POST') {
    $titulo = limpiarDatos($_POST['titulo']);
    $extracto = limpiarDatos($_POST['extracto']);
    $texto = $_POST['texto'];
    $thumb = $_FILES['thumb']['tmp_name'];
    $archivoSubido = '../'.$blog_config['carpetaImg'].$_FILES['thumb']['name'];
    move_uploaded_file($thumb, $archivoSubido);
    $st = $conexion->prepare('INSERT INTO t_articulos (id_articulo, titulo, extracto, texto, thumb) VALUES (null, :titulo, :extracto, :texto, :thumb)');
    $st->execute(array(':titulo' => $titulo, ':extracto' => $extracto, ':texto' => $texto, ':thumb' => $_FILES['thumb']['name']));
    header('Location: '.RUTA.'admin');
}
require '../views/nuevo.view.php';