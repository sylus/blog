<?php
require 'admin/config.php';
require 'functions.php';

$conexion = conexion($bd_config);
if (!$conexion) {
    header('Location: error.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['busqueda'])) {
    $busqueda = limpiarDatos($_GET['busqueda']);
    $st = $conexion->prepare('SELECT * FROM t_articulos WHERE titulo LIKE :busqueda OR texto LIKE :busqueda');
    $st->execute(array('busqueda' => "%$busqueda%"));
    $resultados = $st->fetchAll();
    if (empty($resultados)) {
        $titulo = 'No se econtraron artículos con el criterio: ' . $busqueda;
    } else {
        $titulo = 'Resultados de la búsqueda: ' . $busqueda;
    }
} else {
    header('Location: ' .RUTA. 'index.php');
}
require 'views/buscar.view.php';