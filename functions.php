<?php
function conexion($bd_config){
    try {
        $conexion = new PDO('mysql:host=127.0.0.1; dbname='.$bd_config['basedatos'], $bd_config['usuario'], $bd_config['pass']);
        return $conexion;
    } catch (PDOException $exc) {
        return false;
    }
}

function limpiarDatos($var){
    $var = trim($var);
    $var = stripcslashes($var);
    $var = htmlspecialchars($var);
    return $var;
}

function paginaActual(){
    return isset($_GET['p']) ? (int)$_GET['p'] : 1;
}

function obtenerPost($postPorPag, $conexion){
    $inicio = (paginaActual() > 1) ? paginaActual() * $postPorPag - $postPorPag : 0;
    $st = $conexion->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM t_articulos LIMIT $inicio, $postPorPag");
    $st->execute();
    return $st->fetchAll();
}

function id_articulo($id){
    return (int)  limpiarDatos($id);
}

function obtenerPostPorId($conexion, $id){
   $resultado = $conexion->query("SELECT * FROM t_articulos WHERE id_articulo= $id LIMIT 1");
   $resultado = $resultado->fetchAll();
   return ($resultado) ? $resultado : FALSE;
}

function fecha($fecha){
    $timestamp = strtotime($fecha);
    $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $dia = date('d', $timestamp);
    $mes = date('m', $timestamp)-1;
    $year = date('Y', $timestamp);
    $fecha = "$dia de " . $meses[$mes] . " de $year";
    return $fecha;
}

function numeroPag($postPorPag, $conexion){
    $totalPost = $conexion->prepare('SELECT FOUND_ROWS() as total');
    $totalPost->execute();
    $totalPost = $totalPost->fetch()['total'];
    $numeroPaginas = ceil($totalPost / $postPorPag);
    return $numeroPaginas;
}

function comprobarSesion(){
    if (!isset($_SESSION['admin'])) {
        header('Location: '.RUTA);
    }
}