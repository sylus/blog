<?php
require 'header.php';
?>
<div class="contenedor">
    <div class="post">
        <article>
            <h2 class="titulo">Editar Artículo</h2>
            <form class="formulario" method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <input type="hidden" value="<?php echo $post['id_articulo']; ?>" name="id_articulo">
                <input type="text" name="titulo" value="<?php echo $post['titulo']; ?>">
                <input type="text" name="extracto" value="<?php echo $post['extracto']; ?>">
                <textarea name="texto"><?php echo $post['texto']; ?>"</textarea>
                <input type="file" name="thumb">
                <input type="hidden" value="<?php echo $post['thumb']; ?>" name="thumb-guardada">
                <input type="submit" value="Modificar Artículo">
            </form>
        </article>
    </div>
</div>
<?php
require 'footer.php';
?>